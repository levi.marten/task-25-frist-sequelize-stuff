// Welcome to this code masterpiece which does some cool stuff with SQL databases. Heck yeah

const { Sequelize, QueryTypes } = require('sequelize');

require('dotenv').config();

const sequelize = new Sequelize({
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT
});

async function connect() {

    try {
        await sequelize.authenticate();
        console.log('Connection established...');

        const countries = await sequelize.query('SELECT * FROM country', {
            type: QueryTypes.SELECT
        });
        const cities = await sequelize.query('SELECT * FROM city', {
            type: QueryTypes.SELECT
        });
        const languages = await sequelize.query('SELECT * FROM countrylanguage', {
            type: QueryTypes.SELECT
        });
        console.log(countries[0], cities[0], languages[0]);
    }
    catch (e) {
        console.error(e);
    }
};

connect();